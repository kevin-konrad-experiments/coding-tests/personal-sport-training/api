import {Request, RequestHandler, Response} from "express";
import {ScheduledActivity, Training} from "../models/training.model";
import * as service from '../services/trainings.service';
import * as SportsService from '../services/sports.service';
import {Sport} from "../models/sport.model";

/**
 * Handle data and errors for [GET] /trainings route
 * @param req
 * @param res
 */
const getAll: RequestHandler = async (req: Request, res: Response) => {
  try {
    const trainings: Training[] = await service.getAll();
    return res.status(200).json(trainings);
  } catch (e) {
    return res.status(500).json(e);
  }
};

/**
 * Handle data and errors for [GET] /trainings/:id route
 * @param req
 * @param res
 */
const getById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const training: Training | undefined = await service.getById(req.params.id);

    if (!training) {
      return res.sendStatus(404);
    }

    return res.status(200).json(training);
  } catch (e) {
    return res.status(500).json(e);
  }
};

/**
 * Handle data and errors for route [POST] /trainings
 * @param req
 * @param res
 */
const create: RequestHandler = async (req: Request, res: Response) => {
  try {
    const sport: Sport | undefined = await SportsService.getById(req.body.sport_id);

    if (!sport) {
      return res.sendStatus(404);
    }

    req.body.sport = sport;
    const training: Training = await service.create(req.body);
    return res.status(200).json(training);
  } catch (e) {
    return res.status(500).json(e);
  }
};

/**
 * Handle data and errors for [DELETE] /trainings/:id route
 * @param req
 * @param res
 */
const remove: RequestHandler = async (req: Request, res: Response) => {
  try {
    const trainings: Training[] = await service.remove(req.params.id);

    if (trainings.length === 0) {
      return res.sendStatus(404);
    }

    return res.status(200).json(trainings);
  } catch (e) {
    return res.status(500).json(e);
  }
};

/**
 * Handle data and errors for [PUT] /sports/:id route
 * @param req
 * @param res
 */
const updateActivity: RequestHandler = async (req: Request, res: Response) => {
  try {
    const scheduledActivity: ScheduledActivity | undefined = await service.updateScheduledActivity(req.params.id, req.params.activityId, req.body);

    if (!scheduledActivity) {
      return res.sendStatus(404);
    }

    return res.status(200).json(scheduledActivity);
  } catch (e) {
    return res.status(500).json(e);
  }
};

/**
 * Handle data and errors for route [DELETE] /trainings/:id/activities/:id
 * @param req
 * @param res
 */
const removeActivity = async (req: Request, res: Response) => {
  try {
    const result: ScheduledActivity[] = await service.removeScheduledActivity(req.params.id, req.params.activityId);

    if (result.length === 0) {
      return res.sendStatus(404);
    }

    return res.status(200).json(result);
  } catch (e) {
    return res.status(500).json(e);
  }
};

/**
 * Export all constants for other modules to import it
 */
export {
  getAll,
  getById,
  create,
  remove,
  updateActivity,
  removeActivity,
}
