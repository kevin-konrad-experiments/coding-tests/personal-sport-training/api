import {Request, RequestHandler, Response} from "express";
import * as activitiesService from '../services/activities.service';
import Activity from "../models/activity.model";

/**
 * Handle data and errors for [GET] /activities route
 * @param req
 * @param res
 */
const getAll: RequestHandler = async (req: Request, res: Response) => {
  try {
    const activities: Activity[] = await activitiesService.getAll();
    return res.status(200).json(activities);
  } catch (e) {
    return res.status(500).json(e);
  }
};

/**
 * Export all constants for other modules to import it
 */
export {
  getAll,
}
