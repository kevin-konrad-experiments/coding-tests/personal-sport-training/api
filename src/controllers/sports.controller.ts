import {Request, RequestHandler, Response} from "express";
import * as sportsService from '../services/sports.service';
import {Sport} from "../models/sport.model";

/**
 * Handle data and errors for [GET] /sports route
 * @param req
 * @param res
 */
const getAll: RequestHandler = async (req: Request, res: Response) => {
  try {
    const sports: Sport[] = await sportsService.getAll();
    return res.status(200).json(sports);
  } catch (e) {
    return res.status(500).json(e);
  }
};

/**
 * Handle data and errors for [GET] /sports/:id route
 * @param req
 * @param res
 */
const getById: RequestHandler = async (req: Request, res: Response) => {
  try {
    const sport: Sport | undefined = await sportsService.getById(req.params.id);

    if (!sport) {
      return res.sendStatus(404);
    }

    return res.status(200).json(sport);
  } catch (e) {
    return res.status(500).json(e);
  }
};

/**
 * Handle data and errors for [POST] /sports route
 * @param req
 * @param res
 */
const create: RequestHandler = async (req: Request, res: Response) => {
  try {
    const sport: Sport = await sportsService.create(req.body);
    return res.status(200).json(sport);
  } catch (e) {
    return res.status(500).json(e);
  }
};

/**
 * Handle data and errors for [PUT] /sports/:id route
 * @param req
 * @param res
 */
const update: RequestHandler = async (req: Request, res: Response) => {
  try {
    const sport: Sport | undefined = await sportsService.update(req.params.id, req.body);

    if (!sport) {
      return res.sendStatus(404);
    }

    return res.status(200).json(sport);
  } catch (e) {
    return res.status(500).json(e);
  }
};

/**
 * Handle data and errors for [DELETE] /sports/:id route
 * @param req
 * @param res
 */
const remove: RequestHandler = async (req: Request, res: Response) => {
  try {
    const sports: Sport[] = await sportsService.remove(req.params.id);

    if (sports.length === 0) {
      return res.sendStatus(404);
    }

    return res.status(200).json(sports);
  } catch (e) {
    return res.status(500).json(e);
  }
};

/**
 * Export all constants for other modules to import it
 */
export {
  getAll,
  getById,
  create,
  update,
  remove,
}
