import Activity from "./activity.model";

export interface Sport {
  id: string;
  name: string;
  image_url: string;
  activities: Activity[];
}

export interface SportDto {
  id: string;
  name: string;
  image_url: string;
  activities_ids: string[];
}

export interface SportCreateBody {
  id?: string;
  name: string;
  activities_ids: string[];
}

export interface SportUpdateBody {
  id: string;
  name: string;
  activities_ids: string[];
}
