import Activity from "./activity.model";
import {Sport} from "./sport.model";

export enum TrainingMomentOfDay {
  MORNING,
  AFTERNOON,
  EVENING,
}

export interface TrainingCreateBodyParams {
  id?: string;
  name: string;
  sport: Sport;
  period_start: Date;
  period_end: Date;
  activities_per_day: number;
  weekends_only: boolean;
}

export interface TrainingPeriod {
  start: Date;
  end: Date;
}

export interface ScheduledActivity {
  id: string;
  date: Date;
  moment: TrainingMomentOfDay;
  activity: Activity;
}

export interface Training {
  id: string;
  name: string;
  sport: Sport;
  period: TrainingPeriod;
  activities_per_day: number;
  weekends_only: boolean;
  schedule: ScheduledActivity[];
}
