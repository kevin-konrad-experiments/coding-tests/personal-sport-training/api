import express, {Application} from "express";
import {routes} from "./routes/routes";

/**
 * Port of the express application
 * Could be stored in a separated settings.ts file
 */
const port = process.env.PORT || 3000;

/**
 * Isolate and export the instance of the express application
 * for further usage in unit tests
 */
export const app: Application = express().use(routes);

/**
 * Isolate and export the instance of the HTTP server
 * for further usage in unit tests
 */
export const server = app.listen(port, () => console.log(`API listening on port ${port}`));
