import {NextFunction, Request, Response} from "express";

/**
 * Middleware for validating format of input training parameters
 * @param req
 * @param res
 * @param next
 */
const validateTrainingBody = (req: Request, res: Response, next: NextFunction) => {
  if (!req.body) {
    return res.status(400).send('Invalid request body');
  }

  if (!req.body.hasOwnProperty('sport_id') || typeof req.body.sport_id !== "string") {
    return res.status(400).send('Invalid value for parameter sport_id (string)');
  }

  if (!req.body.hasOwnProperty('name') || typeof req.body.name !== "string") {
    return res.status(400).send('Invalid value for parameter name (string)');
  }

  if (isNaN(Date.parse(req.body.period_start))) {
    return res.status(400).send('Invalid value for parameter period_start (date)');
  }

  if (isNaN(Date.parse(req.body.period_end))) {
    return res.status(400).send('Invalid value for parameter period_end (date)');
  }

  req.body.period_start = new Date(req.body.period_start);
  req.body.period_end = new Date(req.body.period_end);

  if (req.body.period_start.getTime() > req.body.period_end.getTime()) {
    return res.status(400).send('Invalid value for parameter period_start and period_end (start date must be lower or equal to end date)');
  }

  if (req.body.hasOwnProperty('activities_per_day') && typeof req.body.activities_per_day !== "number") {
    return res.status(400).send('Invalid value for parameter activities_per_day (number between 0 and 4)');
  }

  if (req.body.activities_per_day < 1 || req.body.activities_per_day > 3) {
    return res.status(400).send('Invalid value for parameter activities_per_day (number between 0 and 4)');
  }

  if (req.body.hasOwnProperty('weekends_only') && typeof req.body.weekends_only !== "boolean") {
    return res.status(400).send('Invalid value for parameter weekends_only (boolean)');
  }

  next();
};

/**
 * Export all constants for other modules to import it
 */
export {
  validateTrainingBody,
}
