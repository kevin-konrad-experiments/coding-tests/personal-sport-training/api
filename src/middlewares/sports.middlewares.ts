import {NextFunction, Request, Response} from "express";

/**
 * Middleware for validating format of input sport parameters
 * @param req
 * @param res
 * @param next
 */
const validateSportBody = (req: Request, res: Response, next: NextFunction) => {
  if (!req.body) {
    return res.status(400).send('Invalid request body');
  }

  if (!req.body.name || req.body.name.length < 2) {
    return res.status(400).send('Invalid value for parameter name (string length > 1)');
  }

  try {
    req.body.activities_ids = JSON.parse(req.body.activities_ids);
  } catch {
    req.body.activities_ids = [];
  }

  if (!req.body.activities_ids || !Array.isArray(req.body.activities_ids)) {
    return res.status(400).send('Invalid value for parameter activities_ids (array of strings)');
  }

  if (req.file) {
    req.body.image_url = `/images/sports/${req.file.filename}`;
  }

  next();
};

/**
 * Export all constants for other modules to import it
 */
export {
  validateSportBody,
}
