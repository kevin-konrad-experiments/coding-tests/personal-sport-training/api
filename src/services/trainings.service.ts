import {ScheduledActivity, Training, TrainingCreateBodyParams} from "../models/training.model";
import {uuid} from "uuidv4";
import Activity from "../models/activity.model";

/**
 * Objects array related to this service
 * This is just a fake storage for the purpose of the coding test
 */
const trainings: Training[] = [];

/**
 * Retrieve all objects in the objects array
 */
const getAll = (): Promise<Training[]> => {
  return Promise.resolve(trainings);
};

/**
 * Retrieve one object with its id
 * @param id: identifier of the object to be retrieved
 */
const getById = (id: string): Promise<Training | undefined> => {
  const training = trainings.find(training => training.id === id);
  return Promise.resolve(training);
};

/**
 * Insert a new object in the objects array
 * Additionally generates a new uuid if it has not been provided within params
 * @param params: user input for the new object to be inserted
 */
const create = async (params: TrainingCreateBodyParams): Promise<Training> => {
  const training: Training = await createTrainingFromParams(params);
  trainings.push(training);
  return Promise.resolve(training);
};

/**
 * Create a new training object from user input parameters
 * @param params
 */
const createTrainingFromParams = async (params: TrainingCreateBodyParams): Promise<Training> => {
  const schedule: ScheduledActivity[] = createTrainingSchedule(params.period_start, params.period_end, params.weekends_only, params.activities_per_day, params.sport.activities);

  return {
    id: params.id || uuid(),
    name: params.name,
    sport: params.sport,
    period: {start: params.period_start, end: params.period_end},
    activities_per_day: params.activities_per_day != undefined ? params.activities_per_day : 1,
    weekends_only: params.weekends_only != undefined ? params.weekends_only : false,
    schedule: schedule,
  };
};

/**
 * Create an array of scheduled activities from user input params
 * @param periodStart
 * @param periodEnd
 * @param weekendsOnly
 * @param activitiesPerDay
 * @param activities
 */
const createTrainingSchedule = (periodStart: Date, periodEnd: Date, weekendsOnly: boolean, activitiesPerDay: number, activities: Activity[]): ScheduledActivity[] => {
  const schedule: ScheduledActivity[] = [];
  const date = new Date(periodStart);

  // Handle empty activities array
  if (!activities || activities.length === 0) {
    return schedule;
  }

  // Loop through each day between start date and end date
  for (let index = 0; date.getTime() <= periodEnd.getTime(); date.setDate(date.getDate() + 1)) {
    const dayOfWeek: number = date.getDay();

    // Handle case where there are not enough activities to fill the schedule: goes back to index 0
    if (index >= activities.length) {
      index = 0;
    }

    // Handle weekends only
    if (weekendsOnly && dayOfWeek !== 0 && dayOfWeek !== 6) {
      date.setDate(date.getDate() + 1);
      continue;
    }

    // Handle multiple activities per day
    for (let momentOfDay = 0; momentOfDay < activitiesPerDay; momentOfDay++) {

      // Handle case where there are not enough activities to fill the schedule: goes back to index 0
      if (index >= activities.length) {
        index = 0;
      }

      const activity: Activity = activities[index];
      schedule.push({ id: uuid(), activity, date: new Date(date), moment: momentOfDay });
      index++;
    }
  }

  return schedule;
};

/**
 * Delete an existing object from the objects array with its id
 * @param id: identifier of the object to be deleted
 */
const remove = async (id: string): Promise<Training[]> => {
  const index: number = trainings.findIndex(sport => sport.id === id);
  return trainings.splice(index, index !== -1 ? 1 : 0);
};

/**
 * Update an existing scheduled activity with its id from an existing training
 * @param id: identifier of the training
 * @param scheduledActivityId: identifier of the activity to be update
 * @param scheduledActivity: new values for the scheduled activity
 */
const updateScheduledActivity = async (id: string, scheduledActivityId: string, scheduledActivity: ScheduledActivity): Promise<ScheduledActivity | undefined> => {
  const trainingIndex: number = trainings.findIndex(training => training.id === id);

  if (trainingIndex === -1) {
    return Promise.resolve(undefined);
  }

  const training: Training = trainings[trainingIndex];
  const activityIndex: number = training.schedule.findIndex(activity => activity.id === scheduledActivityId);

  if (activityIndex === -1) {
    return Promise.resolve(undefined);
  }

  training.schedule[activityIndex] = {...training.schedule[activityIndex], activity: scheduledActivity.activity};
  return training.schedule[activityIndex];
};

/**
 * Delete an existing scheduled activity with its id from an existing training
 * @param id: identifier of the training
 * @param activityId: identifier of the activity to be deleted
 */
const removeScheduledActivity = async (id: string, activityId: string): Promise<ScheduledActivity[]> => {
  const trainingIndex: number = trainings.findIndex(training => training.id === id);

  if (trainingIndex === -1) {
    return Promise.resolve([]);
  }

  const training: Training = trainings[trainingIndex];
  const activityIndex: number = trainings[trainingIndex].schedule.findIndex(activity => activity.id === activityId);
  return training.schedule.splice(activityIndex, activityIndex !== -1 ? 1 : 0);
};

/**
 * Export all constants for other modules to import it
 */
export {
  getAll,
  getById,
  create,
  remove,
  updateScheduledActivity,
  removeScheduledActivity,
}
