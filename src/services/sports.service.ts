import {Sport, SportDto} from "../models/sport.model";
import * as data from '../database/sports.data.json';
import {uuid} from "uuidv4";
import * as activitiesService from './activities.service';
import Activity from "../models/activity.model";

/**
 * Objects array related to this service
 * This is just a fake storage for the purpose of the coding test
 */
const sports: SportDto[] = data.sports;

/**
 * Retrieve all objects in the objects array
 */
const getAll = async (): Promise<Sport[]> => {
  return await mapDtoArrayToObjects(sports);
};

/**
 * Map a DTO array into an objects array
 * @param dtoArray
 */
const mapDtoArrayToObjects = async (dtoArray: SportDto[]): Promise<Sport[]> => {
  const promises: Promise<Sport>[] = await dtoArray.map(mapDtoToObject);
  return Promise.all(promises);
};

/**
 * Map a DTO object into an object
 * @param dto
 */
const mapDtoToObject = async (dto: SportDto): Promise<Sport> => {
  const activities: Activity[] = await activitiesService.getByIds(dto.activities_ids);

  return {
    id: dto.id,
    name: dto.name,
    image_url: dto.image_url,
    activities,
  };
};

/**
 * Retrieve one object with its id
 * @param id: identifier of the object to be retrieved
 */
const getById = async (id: string): Promise<Sport | undefined> => {
  const dto: SportDto | undefined = sports.find(sport => sport.id === id);

  if (!dto) {
    return Promise.resolve(undefined);
  }

  return await mapDtoToObject(dto);
};

/**
 * Insert a new object in the objects array
 * Additionally generates a new uuid if it has not been provided within params
 * @param dto: new DTO object to be inserted
 */
const create = async (dto: SportDto): Promise<Sport> => {
  dto.id = dto.id || uuid();
  sports.push(dto);
  return await mapDtoToObject(dto);
};

/**
 * Update an existing object with its id and new values passed in params
 * Merges the values of the existing object with the properties of the new object
 * @param id: identifier of the object to be updated
 * @param dto: new values to be applied to the existing object
 */
const update = async (id: string, dto: SportDto): Promise<Sport | undefined> => {
  const index: number = sports.findIndex(sport => sport.id === id);

  if (index === -1) {
    return Promise.resolve(undefined);
  }

  sports[index] = { ...sports[index], name: dto.name, image_url: dto.image_url || sports[index].image_url, activities_ids: dto.activities_ids || [] };
  return await mapDtoToObject(sports[index]);
};

/**
 * Delete an existing object from the objects array with its id
 * @param id: identifier of the object to be deleted
 */
const remove = async (id: string): Promise<Sport[]> => {
  const index: number = sports.findIndex(sport => sport.id === id);
  const dtoArray: SportDto[] = sports.splice(index, index !== -1 ? 1 : 0);
  return await mapDtoArrayToObjects(dtoArray);
};

/**
 * Export all constants for other modules to import it
 */
export {
  getAll,
  getById,
  create,
  update,
  remove,
};
