import * as data from '../database/activities.data.json';
import Activity from "../models/activity.model";

/**
 * Objects array related to this service
 * This is just a fake storage for the purpose of the coding test
 */
const activities: Activity[] = data.activities;

/**
 * Retrieve all objects in the objects array
 */
const getAll = (): Promise<Activity[]> => {
  return Promise.resolve(activities);
};

/**
 * Retrieve one object with its id
 * @param id: identifier of the object to be retrieved
 */
const getById = (id: string): Promise<Activity | undefined> => {
  const activity = activities.find(activity => activity.id === id);
  return Promise.resolve(activity);
};

/**
 * Retrieve multiple objects with their ids
 * @param ids: identifiers array of the objects to be retrieved
 */
const getByIds = (ids: string[]): Promise<Activity[]> => {
  const results = activities.filter(activity => ids.includes(activity.id));
  return Promise.resolve(results);
};

export {
  getAll,
  getById,
  getByIds,
}
