import request from "supertest";
import {app, server} from '../../index';

describe('Static files', () => {

  const image_url: string = '/static/images/sports/rugby.png';

  it('should serve a static sport image', async (done) => {
    const res = await request(app).get(image_url);

    expect(res.status).toEqual(200);
    done()
  });

  afterAll(async () => {
    await server.close();
  });
});
