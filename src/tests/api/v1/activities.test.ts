import request from 'supertest';
import {app, server} from '../../../index';

describe('Activities endpoints', () => {
  const baseUrl: string = '/api/v1/activities';

  describe('[GET] /activities', () => {
    it('should retrieve all activities', async (done) => {
      const res = await request(app).get(baseUrl);

      expect(res.status).toEqual(200);
      expect(res.body).toBeInstanceOf(Array);
      expect(res.body.length).toBeGreaterThan(0);
      done();
    });
  });

  afterAll(async () => {
    await server.close();
  });
});
