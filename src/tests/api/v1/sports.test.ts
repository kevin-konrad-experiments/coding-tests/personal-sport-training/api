import request from 'supertest';
import {app, server} from '../../../index';
import {Sport, SportCreateBody} from "../../../models/sport.model";

describe('Sports endpoints', () => {
  const baseUrl: string = '/api/v1/sports';
  const testSport: SportCreateBody = {id: "test-id", name: 'Test', activities_ids: []};
  const validParameters: SportCreateBody = {name: 'Valid sport', activities_ids: []};
  const wrongId: string = '123456';
  const newSportName = 'New name';

  beforeAll(async (done) => {
    await request(app)
      .post(baseUrl)
      .send(testSport);

    done();
  });

  describe('[GET] /sports', () => {
    it('should retrieve all sports', async (done) => {
      const res = await request(app).get(baseUrl);

      expect(res.status).toEqual(200);
      expect(res.body).toBeInstanceOf(Array);
      expect(res.body.length).toBeGreaterThan(0);
      done();
    });
  });

  describe('[GET] /sports/:id', () => {
    it(`should retrieve sport with good id`, async (done) => {
      const res = await request(app).get(`${baseUrl}/${testSport.id}`);

      expect(res.status).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body).toHaveProperty('id');
      expect(res.body).toHaveProperty('name');
      expect(res.body.id).toEqual(testSport.id);
      expect(res.body.name).toEqual(testSport.name);
      done();
    });

    it(`should fail to retrieve sport with wrong id`, async (done) => {
      const res = await request(app).get(`${baseUrl}/${wrongId}`);

      expect(res.status).toEqual(404);
      done();
    });
  });

  describe('[POST] /sports', function () {
    it('should create a new sport', async (done) => {
      const res = await request(app).post(baseUrl).send(validParameters);

      expect(res.status).toEqual(200);
      expect(res.body).toHaveProperty('id');
      expect(res.body).toHaveProperty('name');
      expect(res.body.name).toEqual(validParameters.name);
      done();
    });

    it('should fail to create a new sport with wrong body', async (done) => {
      const res = await request(app).post(baseUrl).send({test: newSportName});

      expect(res.status).toEqual(400);
      done();
    });
  });

  describe('[PUT] /sports/:id', function () {
    it(`should update sport name with good id`, async (done) => {
      const res = await request(app).put(`${baseUrl}/${testSport.id}`).send({name: newSportName});

      expect(res.status).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body).toHaveProperty('id');
      expect(res.body).toHaveProperty('name');
      expect(res.body.id).toEqual(testSport.id);
      expect(res.body.name).toEqual(newSportName);
      done();
    });

    it(`should fail to update sport name with wrong id`, async (done) => {
      const res = await request(app).put(`${baseUrl}/${wrongId}`).send({name: newSportName});

      expect(res.status).toEqual(404);
      done();
    });

    it(`should not be able to update sport id`, async (done) => {
      const newSport: Sport = {id: wrongId, name: newSportName, image_url: '', activities: []};
      const res = await request(app).put(`${baseUrl}/${testSport.id}`).send(newSport);

      expect(res.status).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body).toHaveProperty('id');
      expect(res.body).toHaveProperty('name');
      expect(res.body.id).toEqual(testSport.id);
      expect(res.body.name).toEqual(newSportName);
      done();
    });
  });

  describe('[DELETE] /sports/:id', function () {
    it(`should delete sport with good id`, async (done) => {
      const res = await request(app).delete(`${baseUrl}/${testSport.id}`);

      expect(res.status).toEqual(200);
      expect(res.body).toBeInstanceOf(Array);
      expect(res.body.length).toEqual(1);
      expect(res.body[0].id).toEqual(testSport.id);
      done();
    });

    it(`should not delete sport with wrong id`, async (done) => {
      const res = await request(app).delete(`${baseUrl}/${wrongId}`);

      expect(res.status).toEqual(404);
      done();
    });
  });

  afterAll(async () => {
    await server.close();
  });
});
