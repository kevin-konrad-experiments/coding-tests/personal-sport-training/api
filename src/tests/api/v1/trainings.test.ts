import request from "supertest";
import {app, server} from "../../../index";
import {Sport} from "../../../models/sport.model";
import {ScheduledActivity} from "../../../models/training.model";

describe('Trainings endpoints', function () {
  const wrongId: string = 'wrong-id';
  const baseUrl: string = '/api/v1/trainings';
  const testSport: Sport = {
    id: 'dfa7ae82-5da1-4212-ba2e-f4859aa8197d',
    name: 'Football',
    image_url: '',
    activities: [],
  };
  const testTraining = {
    id: 'test',
    sport_id: testSport.id,
    name: 'Test training',
    period_start: new Date(2020, 1, 1).toISOString(),
    period_end: new Date(2020, 2, 1).toISOString(),
    activities_per_day: 1,
    weekends_only: false,
  };
  const validParams = {
    sport_id: testSport.id,
    name: 'Test training',
    period_start: new Date(2020, 0, 1).toISOString(),
    period_end: new Date(2020, 1, 1).toISOString(),
    activities_per_day: 1,
    weekends_only: false,
  };

  beforeAll(async (done) => {
    await request(app)
      .post(baseUrl)
      .send(testTraining);

    done();
  });

  describe('[GET] /trainings', function () {
    it('should retrieve all the trainings', async (done) => {
      const res = await request(app).get(baseUrl);

      expect(res.status).toEqual(200);
      expect(res.body).toBeInstanceOf(Array);
      done();
    });
  });

  describe('[GET] /trainings/:id', () => {
    it(`should retrieve training with good id`, async (done) => {
      const res = await request(app).get(`${baseUrl}/${testTraining.id}`);

      expect(res.status).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body).toHaveProperty('id');
      expect(res.body).toHaveProperty('name');
      expect(res.body.id).toEqual(testTraining.id);
      expect(res.body.name).toEqual(testTraining.name);
      done();
    });

    it(`should fail to retrieve sport with wrong id`, async (done) => {
      const res = await request(app).get(`${baseUrl}/${wrongId}`);

      expect(res.status).toEqual(404);
      done();
    });
  });

  describe('[POST] /trainings', function () {
    it('should create a new training with good params', async function (done) {
      const res = await request(app).post(baseUrl).send(validParams);

      expect(res.status).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body).toHaveProperty('id');
      expect(res.body).toHaveProperty('sport');
      expect(res.body).toHaveProperty('period');
      expect(res.body).toHaveProperty('activities_per_day');
      expect(res.body).toHaveProperty('weekends_only');
      expect(res.body).toHaveProperty('schedule');
      expect(res.body.schedule).toBeInstanceOf(Array);
      expect(res.body.schedule.length).toEqual(32);
      done();
    });

    it('should create a new training with activities only scheduled on weekend days', async function (done) {
      const res = await request(app).post(baseUrl).send({...validParams, weekends_only: true});

      expect(res.status).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body).toHaveProperty('id');
      expect(res.body).toHaveProperty('sport');
      expect(res.body).toHaveProperty('period');
      expect(res.body).toHaveProperty('activities_per_day');
      expect(res.body).toHaveProperty('weekends_only');
      expect(res.body).toHaveProperty('schedule');
      expect(res.body.schedule).toBeInstanceOf(Array);

      for (let i = 0; i < res.body.schedule.length; i++) {
        const activity: ScheduledActivity = res.body.schedule[i];
        const date: Date = new Date(activity.date);
        expect(date.getDay() === 0 || date.getDay() === 6).toBeTruthy();
      }

      done();
    });

    it('should fail to create a new training with wrong sport id', async function (done) {
      const res = await request(app).post(baseUrl).send({...validParams, sport_id: 'string value'});

      expect(res.status).toEqual(404);
      done();
    });

    it('should fail to create a new training with wrong period_start value', async function (done) {
      const res = await request(app).post(baseUrl).send({...validParams, period_start: 'test'});

      expect(res.status).toEqual(400);
      done();
    });

    it('should fail to create a new training with wrong period_end value', async function (done) {
      const res = await request(app).post(baseUrl).send({...validParams, period_end: 'test'});

      expect(res.status).toEqual(400);
      done();
    });

    it('should fail to create a new training with wrong activities_per_day value', async function (done) {
      const res = await request(app).post(baseUrl).send({...validParams, activities_per_day: 'string value'});

      expect(res.status).toEqual(400);
      done();
    });

    it('should fail to create a new training with wrong weekends_only value', async function (done) {
      const res = await request(app).post(baseUrl).send({...validParams, weekends_only: 'string value'});

      expect(res.status).toEqual(400);
      done();
    });

    it('should fail to create a new training with start date superior to end date', async function (done) {
      const res = await request(app).post(baseUrl).send({...validParams, period_start: new Date(2020, 2, 1).toISOString(), period_end: new Date(2020, 1, 1).toISOString()});

      expect(res.status).toEqual(400);
      done();
    });
  });

  afterAll(async () => {
    await server.close();
  });
});
