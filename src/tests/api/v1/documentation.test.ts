import request from 'supertest';
import {app, server} from '../../../index';

describe('Documentation endpoints', () => {
  const baseUrl: string = '/api/v1/documentation/';

  describe('[GET] /documentation/', () => {
    it('should retrieve swagger documentation main page', async (done) => {
      const res = await request(app).get(baseUrl);

      expect(res.status).toEqual(200);
      done();
    });
  });

  afterAll(async () => {
    await server.close();
  });
});
