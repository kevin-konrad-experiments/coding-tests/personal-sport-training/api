import express, {Router} from 'express';
import {apiRoutes} from "./api/api.routes";

/**
 * Load all API routes and eventual side routes we might need
 * such as health checker, static files, monitoring, etc...
 */
export const routes: Router = Router()
  .use('/static', express.static('static'))
  .use('/api', apiRoutes);
