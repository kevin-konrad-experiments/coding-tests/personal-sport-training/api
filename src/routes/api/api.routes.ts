import {Router} from 'express';
import cors from 'cors';
import compression from 'compression';
import {v1Routes} from "./v1";
import bodyParser from "body-parser";
import helmet from "helmet";

/**
 * Global middleware configuration for all API routes
 */
export const apiRoutes: Router = Router()
  .use(cors({origin: true}))
  .use(bodyParser.json())
  .use(compression())
  .use(helmet())
  .use('/v1', v1Routes);
