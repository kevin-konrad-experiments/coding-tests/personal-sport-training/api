import {Router} from 'express';
import multer from 'multer';
import * as controller from '../../../controllers/sports.controller';
import * as middlewares from '../../../middlewares/sports.middlewares';

const upload = multer({dest: 'static/images/sports'});

/**
 * Routes declaration for feature `sports`
 * This is just a regular CRUD for the purpose of the coding test
 */
export const sportsRoutes: Router = Router()
  .get('/', controller.getAll)
  .get('/:id', controller.getById)
  .post('/',
    upload.single('image'),
    middlewares.validateSportBody,
    controller.create,
  )
  .put('/:id',
    upload.single('image'),
    middlewares.validateSportBody,
    controller.update,
  )
  .delete('/:id', controller.remove);
