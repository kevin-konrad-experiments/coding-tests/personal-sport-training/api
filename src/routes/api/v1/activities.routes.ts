import {Router} from 'express';
import * as controller from '../../../controllers/activities.controller';

/**
 * Routes declaration for feature `activities`
 */
export const activitiesRoutes: Router = Router()
  .get('/', controller.getAll);
