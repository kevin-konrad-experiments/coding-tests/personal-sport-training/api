import {Router} from 'express';
import {activitiesRoutes} from "./activities.routes";
import {sportsRoutes} from "./sports.routes";
import {trainingsRoutes} from "./trainings.routes";
import swaggerUi from "swagger-ui-express";
import * as swaggerDefinition from "./swagger.json";

/**
 * Load each feature module for API v1
 */
export const v1Routes: Router = Router()
  .use('/documentation', swaggerUi.serve, swaggerUi.setup(swaggerDefinition))
  .use('/activities', activitiesRoutes)
  .use('/sports', sportsRoutes)
  .use('/trainings', trainingsRoutes);
