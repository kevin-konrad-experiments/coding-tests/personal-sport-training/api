import {Router} from 'express';
import * as controller from '../../../controllers/trainings.controller';
import * as middlewares from "../../../middlewares/trainings.middlewares";

/**
 * Routes declaration for feature `trainings`
 */
export const trainingsRoutes: Router = Router()
  .get('/', controller.getAll)
  .get('/:id', controller.getById)
  .post('/', middlewares.validateTrainingBody, controller.create)
  .delete('/:id', controller.remove)
  .put('/:id/activities/:activityId', controller.updateActivity)
  .delete('/:id/activities/:activityId', controller.removeActivity);
