module.exports = {
  clearMocks: true,
  coverageDirectory: "coverage",
  roots: [
    "dist/",
  ],
  testEnvironment: "node",
  verbose: true,
};
