# Personal Sport Training API

This project is coded with TypeScript and Express.js with a clean architecture pattern (routes, controllers, models, services).

Endpoints tests are implemented with Jest and Supertest.

### Run the project
Clone the project: `git clone https://gitlab.com/kevin-konrad-experiments/coding-tests/personal-sport-training/api`

Run the project: `cd api/ && npm i && npm test && npm run build && npm start`

### Run tests
Clone the project: `git clone https://gitlab.com/kevin-konrad-experiments/coding-tests/personal-sport-training/api`

Run tests: `cd api/ && npm i && npm run build && npm test`

### Documentation
Access API documentation at: http://localhost:3000/api/v1/documentation
